## Account manager service

### HTTP API
#### GET account balances
*URL*: `http://<host>:<port>/accounts/<account number>`

*Request call sample*: `http://localhost:8080/accounts/88888888`

##### Response Sample
```
{
    "accountId":2,
    "accountNumber":"88888888",
    "balance":1000000,
    "currency":"HKD"
}
```

*Curl call*: `curl -iXGET http://localhost:8080/accounts/88888888`

#### Transfer funds between accounts
*URL*: `http://<host>:<port>/accounts/<account number>/transfers`

*Request call sample*: `http://localhost:8080/accounts/12345678/transfers`

##### Request payload sample
```
{
    "targetAccountNumber": "88888888", 
    "amount": "123000", 
    "currency": "HKD"
}
```
##### Response sample
```
{"accountId":1,"accountNumber":"12345678","newBalanceAmount":877000,"currency":"HKD"}
```

*Curl call*: `curl -iXPOST http://localhost:8080/accounts/12345678/transfers \
-H 'Content-Type: application/json' \
-d '{"targetAccountNumber": "88888888", "amount": "123000", "currency": "HKD"}'`

### Build & test
`./gradlew clean test`

### Running the service

`./gradlew clean bootRun`

### Run with Docker
`docker run  -p 8080:8080 -e "SPRING_DATASOURCE_URL=jdbc:h2:file:/app/data/acmebank" -v ~/host_dir:/app/data --name account-manager acmebank/account-manager`

### Note 
First service run will initialize a test database with two bank accounts.
For subsequent runs the service must be started with the following environment variable settings:
- `SPRING_DATASOURCE_INITIALIZATION-MODE=never`

#### Launch from commandline
```
export SPRING_DATASOURCE_INITIALIZATION_MODE="never" & \
./gradlew bootRun
```

This is needed to avoid re-creating the test database.