FROM openjdk:8-jre-alpine

ADD account-manager.jar /app/account_manager.jar
RUN mkdir -p /app/data
RUN chown 9999:9999 /app/data

EXPOSE 8080

USER 9999:9999

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Xmx512m -jar /app/account_manager.jar" ]