package acmebank.accountsservice.accounts.controllers

import acmebank.accountsservice.accounts.daos.BankAccount
import acmebank.accountsservice.accounts.models.TransferAmountRequest
import acmebank.accountsservice.accounts.repositories.AccountsRepository
import acmebank.accountsservice.accounts.services.BankAccountService
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.math.BigDecimal
import java.time.Instant
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class CustomerBankAccountOperationsControllerTests {
    @Autowired
    lateinit var mvc: MockMvc

    @Autowired
    lateinit var bankAccountService: BankAccountService

    @Autowired
    lateinit var accountsRepository: AccountsRepository

    @Before
    fun setUp() {
        accountsRepository.save(
            BankAccount(
                userId = 1,
                accountNumber = "12345678",
                balance = BigDecimal.valueOf(1000000),
                currency = "HKD",
                creationTs = Date.from(Instant.now())
            )
        )

        accountsRepository.save(
            BankAccount(
                userId = 2,
                accountNumber = "88888888",
                balance = BigDecimal.valueOf(1000000),
                currency = "HKD",
                creationTs = Date.from(Instant.now())
            )
        )
    }

    @After
    fun tearDown() {
        accountsRepository.deleteAll()
    }

    @Test
    fun `should return account balance for account 12345678`() {
        val apiResponse = mvc.perform(
            MockMvcRequestBuilders.get("/accounts/12345678")
        ).andExpect(status().is2xxSuccessful)
            .andExpect(content().contentType("application/json"))
            .andExpect(content().contentType("application/json"))
            .andExpect(jsonPath("$.accountId").isNotEmpty)
            .andExpect(jsonPath("$.accountNumber").value("12345678"))
            .andExpect(jsonPath("$.balance").value(1000000))
            .andExpect(jsonPath("$.currency").value("HKD"))
    }

    @Test
    fun `should return account balance for account 88888888`() {
        mvc.perform(
            MockMvcRequestBuilders.get("/accounts/88888888")
        ).andExpect(status().is2xxSuccessful)
            .andExpect(content().contentType("application/json"))
            .andExpect(jsonPath("$.accountId").isNotEmpty)
            .andExpect(jsonPath("$.accountNumber").value("88888888"))
            .andExpect(jsonPath("$.balance").value(1000000))
            .andExpect(jsonPath("$.currency").value("HKD"))
    }

    @Test
    fun `should return 404 when querying for undefined accounts`() {
        mvc.perform(
            MockMvcRequestBuilders.get("/accounts/111")
        ).andExpect(status().isNotFound)
    }

    @Test
    fun `should transfer funds from customer account to target account`() {
        val transferRequest = TransferAmountRequest(
            targetAccountNumber = "88888888",
            amount = BigDecimal.valueOf(50000),
            currency = "HKD"
        )

        mvc.perform(
            MockMvcRequestBuilders.post("/accounts/12345678/transfers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapper().writeValueAsString(transferRequest))
        ).andExpect(status().is2xxSuccessful)
            .andExpect(content().contentType("application/json"))
            .andExpect(jsonPath("$.accountId").isNotEmpty)
            .andExpect(jsonPath("$.accountNumber").value("12345678"))
            .andExpect(jsonPath("$.newBalanceAmount").value(950000))
            .andExpect(jsonPath("$.currency").value("HKD"))
    }

    @Test
    fun `should return BAD_REQUEST response if source account does not have sufficient funds for transfer`() {
        val transferRequest = TransferAmountRequest(
            targetAccountNumber = "88888888",
            amount = BigDecimal.valueOf(3000000),
            currency = "HKD"
        )

        mvc.perform(
            MockMvcRequestBuilders.post("/accounts/12345678/transfers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapper().writeValueAsString(transferRequest))
        ).andExpect(status().isBadRequest)
    }

    @Test
    fun `should return BAD_REQUEST response when attempting to transfer to same account`() {
        val transferRequest = TransferAmountRequest(
            targetAccountNumber = "88888888",
            amount = BigDecimal.valueOf(3000),
            currency = "HKD"
        )

        mvc.perform(
            MockMvcRequestBuilders.post("/accounts/88888888/transfers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapper().writeValueAsString(transferRequest))
        ).andExpect(status().isBadRequest)
    }

    @Test
    fun `should return INTERNAL_SERVER_ERROR response if source account is invalid`() {
        val transferRequest = TransferAmountRequest(
            targetAccountNumber = "88888888",
            amount = BigDecimal.valueOf(3000000),
            currency = "HKD"
        )

        mvc.perform(
            MockMvcRequestBuilders.post("/accounts/111/transfers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapper().writeValueAsString(transferRequest))
        ).andExpect(status().isInternalServerError)
    }

    @Test
    fun `should return INTERNAL_SERVER_ERROR response if target account is invalid`() {
        val transferRequest = TransferAmountRequest(
            targetAccountNumber = "111",
            amount = BigDecimal.valueOf(3000000),
            currency = "HKD"
        )

        mvc.perform(
            MockMvcRequestBuilders.post("/accounts/12345678/transfers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapper().writeValueAsString(transferRequest))
        ).andExpect(status().isInternalServerError)
    }
}