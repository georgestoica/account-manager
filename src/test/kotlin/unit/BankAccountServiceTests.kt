package unit

import acmebank.accountsservice.accounts.TransactionLockManager
import acmebank.accountsservice.accounts.daos.BankAccount
import acmebank.accountsservice.accounts.daos.Transaction
import acmebank.accountsservice.accounts.exceptions.InsufficientFundsException
import acmebank.accountsservice.accounts.exceptions.TransaferFailedException
import acmebank.accountsservice.accounts.repositories.AccountsRepository
import acmebank.accountsservice.accounts.repositories.TransactionsRepository
import acmebank.accountsservice.accounts.services.AcmeBankAccountService
import acmebank.accountsservice.accounts.services.TransferValidator
import com.nhaarman.mockitokotlin2.*
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations.initMocks
import java.math.BigDecimal
import java.time.Instant
import java.util.*
import java.util.concurrent.ConcurrentHashMap

class BankAccountServiceTests {
    @Mock
    lateinit var accountsRepo: AccountsRepository

    @Mock
    lateinit var transactionsRepo: TransactionsRepository

    @Mock
    lateinit var transferValidator: TransferValidator

    @Before
    fun setUp() {
        initMocks(this)
    }

    @After
    fun tearDown() {
        reset(accountsRepo, transactionsRepo, transferValidator)
    }

    @Test
    fun `should transfer funds from one account to another` () {
        val testTs = Date.from(Instant.now())

        given(accountsRepo.findByAccountNumber(eq("12345678")))
            .willReturn(BankAccount(
                accountId = 1,
                accountNumber = "12345678",
                userId = 1,
                balance = BigDecimal.valueOf(1000000),
                currency = "HKD",
                creationTs = testTs
            ))

        given(accountsRepo.findByAccountNumber(eq("88888888")))
            .willReturn(BankAccount(
                accountId = 1,
                accountNumber = "88888888",
                userId = 1,
                balance = BigDecimal.valueOf(1000000),
                currency = "HKD",
                creationTs = testTs
            ))

        val underTest = AcmeBankAccountService(accountsRepo,
            transactionsRepo,
            transferValidator,
            TransactionLockManager(ConcurrentHashMap())
        )

        val result = underTest.transferFunds("12345678",
            "88888888",
            BigDecimal.valueOf(50000),
            "HKD"
        )

        assertEquals("12345678", result?.accountNumber)
        assertEquals(BigDecimal.valueOf(950000), result?.balance)
        assertEquals("HKD", result?.currency)

        verify(accountsRepo, times(2)).save(any<BankAccount>())
        verify(transactionsRepo).save(any<Transaction>())
    }

    @Test(expected = TransaferFailedException::class)
    fun `should fail to transfer funds if source account not found` () {
        given(accountsRepo.findByAccountNumber(eq("12345678")))
            .willReturn(null)

        val underTest = AcmeBankAccountService(
            accountsRepo,
            transactionsRepo,
            transferValidator,
            TransactionLockManager(ConcurrentHashMap())
        )

        underTest.transferFunds("12345678",
            "88888888",
            BigDecimal.valueOf(50000),
            "HKD"
        )
    }

    @Test(expected = TransaferFailedException::class)
    fun `should fail to transfer funds if target account not found` () {
        given(accountsRepo.findByAccountNumber(eq("12345678")))
            .willReturn(BankAccount(
                accountId = 1,
                accountNumber = "12345678",
                userId = 1,
                balance = BigDecimal.valueOf(1000000),
                currency = "HKD",
                creationTs = Date.from(Instant.now())
            ))

        given(accountsRepo.findByAccountNumber(eq("88888888")))
            .willReturn(null)

        val underTest = AcmeBankAccountService(
            accountsRepo,
            transactionsRepo,
            transferValidator,
            TransactionLockManager(ConcurrentHashMap())
        )

        underTest.transferFunds("12345678",
            "88888888",
            BigDecimal.valueOf(50000),
            "HKD"
        )
    }

    @Test(expected = InsufficientFundsException::class)
    fun `should fail transfer if source has insufficient funds` () {
        val testTs = Date.from(Instant.now())

        given(transferValidator.validate(any(), any(), any())).willThrow(InsufficientFundsException("12345678"))

        given(accountsRepo.findByAccountNumber(eq("12345678")))
            .willReturn(BankAccount(
                accountId = 1,
                accountNumber = "12345678",
                userId = 1,
                balance = BigDecimal.valueOf(1000),
                currency = "HKD",
                creationTs = testTs
            ))

        given(accountsRepo.findByAccountNumber(eq("88888888")))
            .willReturn(BankAccount(
                accountId = 1,
                accountNumber = "88888888",
                userId = 1,
                balance = BigDecimal.valueOf(1000000),
                currency = "HKD",
                creationTs = testTs
            ))

        val underTest = AcmeBankAccountService(
            accountsRepo,
            transactionsRepo,
            transferValidator,
            TransactionLockManager(ConcurrentHashMap())
        )

        val result = underTest.transferFunds("12345678",
            "88888888",
            BigDecimal.valueOf(50000),
            "HKD"
        )
    }
}