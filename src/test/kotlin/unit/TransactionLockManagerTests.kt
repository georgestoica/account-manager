package unit

import acmebank.accountsservice.accounts.TransactionLockManager
import org.junit.Assert.assertTrue
import org.junit.Test
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock

class TransactionLockManagerTests {
    @Test
    fun `should execute func for account number when no lock exists` () {
        val underTest = TransactionLockManager(ConcurrentHashMap<String, Lock>())

        var result = underTest.executeSync<Boolean>("12345678", 100) {
            true
        }

        Thread.sleep(500)

        assertTrue(result ?: false)
    }

    @Test
    fun `should execute func for account number when lock exists` () {
        val store = ConcurrentHashMap<String, Lock>()
        store.put("12345678", ReentrantLock())

        val underTest = TransactionLockManager(store)

        var result = underTest.executeSync("12345678", 100) {
            true
        }

        Thread.sleep(500)

        assertTrue(result ?: false)
    }

//    @Test
//    fun `should get transaction lock for accountNumber` () {
//        val underTest = TransactionLockManager(hashSetOf<String>())
//
//        val locked = underTest.lock("12345678")
//
//        assertTrue(locked)
//    }
//
//    @Test
//    fun `should release account lock` () {
//        val underTest = TransactionLockManager(hashSetOf<String>())
//
//        underTest.lock("12345678")
//        val released = underTest.release("12345678")
//
//        assertTrue(released)
//    }
//
//    @Test
//    fun `should fail to get lock if already taken` () {
//        val underTest = TransactionLockManager(hashSetOf<String>("12345678"))
//
//        val locked = underTest.lock("12345678")
//
//        assertFalse(locked)
//    }
}