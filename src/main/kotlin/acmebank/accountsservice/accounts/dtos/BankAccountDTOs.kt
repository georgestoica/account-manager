package acmebank.accountsservice.accounts.dtos

import java.math.BigDecimal

data class BankAccountData(
    val accountId: Int,
    val accountNumber: String,
    val balance: BigDecimal,
    val currency: String
)