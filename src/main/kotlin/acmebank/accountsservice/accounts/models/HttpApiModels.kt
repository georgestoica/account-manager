package acmebank.accountsservice.accounts.models

import java.math.BigDecimal

data class GetBalanceResponse(val accountId: Int, val accountNumber: String, val balance: BigDecimal, val currency: String)

data class TransferAmountRequest(val targetAccountNumber: String, val amount: BigDecimal, val currency: String)
data class TransferAmountResponse(val accountId: Int, val accountNumber: String, val newBalanceAmount: BigDecimal, val currency: String)