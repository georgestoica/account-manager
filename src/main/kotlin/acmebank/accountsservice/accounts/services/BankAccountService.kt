package acmebank.accountsservice.accounts.services

import acmebank.accountsservice.accounts.TransactionLockManager
import acmebank.accountsservice.accounts.daos.BankAccount
import acmebank.accountsservice.accounts.daos.Transaction
import acmebank.accountsservice.accounts.daos.TransactionType
import acmebank.accountsservice.accounts.dtos.BankAccountData
import acmebank.accountsservice.accounts.exceptions.CannotTransferToSameAccount
import acmebank.accountsservice.accounts.exceptions.InsufficientFundsException
import acmebank.accountsservice.accounts.exceptions.TransaferFailedException
import acmebank.accountsservice.accounts.repositories.AccountsRepository
import acmebank.accountsservice.accounts.repositories.TransactionsRepository
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import java.time.Instant
import java.util.*

interface BankAccountService {
    fun getAccountBalance(accountNumber: String): BankAccountData?
    fun transferFunds(
        sourceAccountNumber: String,
        targetAccountNumber: String,
        amount: BigDecimal,
        currency: String
    ): BankAccountData?
}

@Service
class AcmeBankAccountService(
    private val accountsRepository: AccountsRepository,
    private val transactionsRepository: TransactionsRepository,
    private val transferValidator: TransferValidator,
    private val transactionLockManager: TransactionLockManager
) : BankAccountService {
    override fun getAccountBalance(accountNumber: String): BankAccountData? {
        return accountsRepository.findByAccountNumber(accountNumber)?.let {
            BankAccountData(
                accountId = it.accountId!!,
                accountNumber = it.accountNumber,
                balance = it.balance,
                currency = it.currency
            )
        }
    }

    @Transactional
    override fun transferFunds(
        sourceAccountNumber: String,
        targetAccountNumber: String,
        amount: BigDecimal,
        currency: String
    ): BankAccountData? {
        return transactionLockManager.executeSync(sourceAccountNumber, 2000) {
            val sourceAccount = accountsRepository.findByAccountNumber(sourceAccountNumber)

            val srcAccountAfterTransfer = sourceAccount?.let { src ->
                val targetAccount = accountsRepository.findByAccountNumber(targetAccountNumber)

                targetAccount?.let { target ->


                    // validate transfer then update accounts

                    transferValidator.validate(src, target, amount)

                    val updatedSourceAccount = src.copy(
                        balance = src.balance - amount,
                        lastUpdateTs = Date.from(Instant.now())
                    )

                    val updatedTargetAccount = target.copy(
                        balance = target.balance + amount,
                        lastUpdateTs = Date.from(Instant.now())
                    )

                    accountsRepository.save(updatedSourceAccount)
                    accountsRepository.save(updatedTargetAccount)

                    // insert transfer transaction

                    transactionsRepository.save(
                        Transaction(
                            accountId = src.accountId!!,
                            targetAccountId = target.accountId,
                            amount = amount,
                            transactionType = TransactionType.TRANSFER,
                            creationTs = Date.from(Instant.now())
                        )
                    )

                    BankAccountData(
                        accountId = updatedSourceAccount.accountId!!,
                        accountNumber = updatedSourceAccount.accountNumber,
                        balance = updatedSourceAccount.balance,
                        currency = updatedSourceAccount.currency
                    )
                }
            }

            when(srcAccountAfterTransfer) {
                null -> throw TransaferFailedException("Failed to transfer funds from account: $sourceAccountNumber to account: $targetAccountNumber")
                else -> srcAccountAfterTransfer
            }
        }

//        val sourceAccount = accountsRepository.findByAccountNumber(sourceAccountNumber)
//
//        sourceAccount?.let { src ->
//            val targetAccount = accountsRepository.findByAccountNumber(targetAccountNumber)
//
//            targetAccount?.let { target ->
//
//
//                // validate transfer then update accounts
//
//                transferValidator.validate(src, target, amount)
//
//                val updatedSourceAccount = src.copy(
//                    balance = src.balance - amount,
//                    lastUpdateTs = Date.from(Instant.now())
//                )
//
//                val updatedTargetAccount = target.copy(
//                    balance = target.balance + amount,
//                    lastUpdateTs = Date.from(Instant.now())
//                )
//
//                accountsRepository.save(updatedSourceAccount)
//                accountsRepository.save(updatedTargetAccount)
//
//                // insert transfer transaction
//
//                transactionsRepository.save(
//                    Transaction(
//                        accountId = src.accountId!!,
//                        targetAccountId = target.accountId,
//                        amount = amount,
//                        transactionType = TransactionType.TRANSFER,
//                        creationTs = Date.from(Instant.now())
//                    )
//                )
//
//                return BankAccountData(
//                    accountId = updatedSourceAccount.accountId!!,
//                    accountNumber = updatedSourceAccount.accountNumber,
//                    balance = updatedSourceAccount.balance,
//                    currency = updatedSourceAccount.currency
//                )
//            }
//        }
//
//        throw TransaferFailedException("Failed to transfer funds from account: $sourceAccountNumber to account: $targetAccountNumber")
    }
}

// Validator

interface TransferValidator {
    fun validate(source: BankAccount, target: BankAccount, amount: BigDecimal)
}

@Component
class AcmeBankTransferValidator : TransferValidator {
    override fun validate(source: BankAccount, target: BankAccount, amount: BigDecimal) {
        when {
            source.accountNumber == target.accountNumber -> throw CannotTransferToSameAccount(source.accountNumber)
            source.balance < amount -> throw InsufficientFundsException(source.accountNumber)
        }
    }
}
