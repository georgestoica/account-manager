package acmebank.accountsservice.accounts.repositories

import acmebank.accountsservice.accounts.daos.BankAccount
import acmebank.accountsservice.accounts.daos.Transaction
import acmebank.accountsservice.accounts.daos.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository("AccountsRepository")
interface AccountsRepository: JpaRepository<BankAccount, Int> {
    fun findByAccountNumber(accountNumber: String): BankAccount?
}

@Repository("TransactionsRepository")
interface TransactionsRepository: JpaRepository<Transaction, Int> {
    fun findByAccountId(accountId: Int): Transaction?
}

@Repository("UsersRepository")
interface UsersRepository: JpaRepository<User, Int>
