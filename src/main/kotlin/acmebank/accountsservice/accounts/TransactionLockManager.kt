package acmebank.accountsservice.accounts

import org.springframework.stereotype.Component
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock

/**
 * Handles concurrent updates on same account.
 * For transfers lock only source account to avoid transferring when the balance is
 * insufficient.
 * TODO: implement lock cleanup
 */
class TransactionLockManager(private val lockStore: ConcurrentHashMap<String, Lock>) {
    fun <T> executeSync(accountNumber: String, timeoutMs: Long? = 5000, executeBlock: () -> T?): T? {
        val accountLock = lockStore.getOrPut(accountNumber, { ReentrantLock() })

        accountLock.tryLock(timeoutMs!!, TimeUnit.MILLISECONDS)

        try {
            return executeBlock()
        } finally {
            accountLock.unlock()
        }
    }
}