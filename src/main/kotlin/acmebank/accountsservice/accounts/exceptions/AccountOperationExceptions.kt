package acmebank.accountsservice.accounts.exceptions

class TransaferFailedException(message: String) : RuntimeException(message)
class InsufficientFundsException(accountNumber: String): RuntimeException("Account $accountNumber does not have sufficient funds")
class CannotTransferToSameAccount(accountNumber: String): RuntimeException("Transfers to own account not permitted for account $accountNumber")