package acmebank.accountsservice.accounts.daos

import java.math.BigDecimal
import java.util.*
import javax.persistence.*

@Entity
@Table(
    name = "accounts", indexes = [
        Index(name = "idx_account_number", columnList = "account_number", unique = true)
    ]
)
data class BankAccount(
    @Id
    @Column(name = "account_id", unique = true, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val accountId: Int? = null,

    @Column(name = "account_number")
    val accountNumber: String,

    @Column(name = "user_id")
    val userId: Int,

    @Column(name = "balance")
    val balance: BigDecimal,

    @Column(name = "currency")
    val currency: String,

    @Column(name = "creation_ts")
    val creationTs: Date,

    @Column(name = "last_update_ts")
    val lastUpdateTs: Date? = null
)

enum class TransactionType{
    DEPOSIT,
    WITHDRAWAL,
    TRANSFER
}

@Entity
@Table(
    name = "transactions", indexes = [
        Index(name = "idx_trx_by_account", columnList = "account_id,trx_type")
    ]
)
data class Transaction(
    @Id
    @Column(name = "trx_id", unique = true, insertable = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val transactionId: Int? = null,

    @Column(name = "account_id")
    val accountId: Int,

    @Column(name = "trx_type")
    val transactionType: TransactionType,

    @Column(name = "amount")
    val amount: BigDecimal,

    @Column(name = "target_account_id")
    val targetAccountId: Int?,

    @Column(name = "creation_ts")
    val creationTs: Date
)

@Entity
@Table(
    name = "users", indexes = [
        Index(name = "idx_users", columnList = "username")
    ]
)
data class User(
    @Id
    @Column(name = "user_id", unique = true, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val userId: Int? = null,

    @Column(name = "username")
    val username: String,

    @Column(name = "password")
    val password: String,

    @Column(name = "email")
    val email: String,

    @Column(name = "first_name")
    val firstName: String,

    @Column(name = "last_name")
    val lastName: String,

    @Column(name = "address")
    val address: String,

    @Column(name = "phone_number")
    val phoneNumber: String,

    @Column(name = "creation_ts")
    val creationTs: Date
)

// Validations

fun BankAccount.validate(): Boolean {
    return accountId != null &&
            !accountNumber.isBlank() &&
            userId != 0 &&
            balance >= BigDecimal.ZERO &&
            !currency.isBlank()
}