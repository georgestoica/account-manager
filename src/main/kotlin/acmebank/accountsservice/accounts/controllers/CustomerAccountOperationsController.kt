package acmebank.accountsservice.accounts.controllers

import acmebank.accountsservice.accounts.exceptions.CannotTransferToSameAccount
import acmebank.accountsservice.accounts.exceptions.InsufficientFundsException
import acmebank.accountsservice.accounts.exceptions.TransaferFailedException
import acmebank.accountsservice.accounts.models.GetBalanceResponse
import acmebank.accountsservice.accounts.models.TransferAmountRequest
import acmebank.accountsservice.accounts.models.TransferAmountResponse
import acmebank.accountsservice.accounts.services.BankAccountService
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/accounts", produces = [MediaType.APPLICATION_JSON_VALUE])
class CustomerAccountOperationsController(
    private val bankAccountService: BankAccountService
) {

    @GetMapping(path = ["/{accountNumber}"])
    fun getAccountBalance(@PathVariable("accountNumber") accountNumber: String): ResponseEntity<GetBalanceResponse> {
        val bankAccountData = bankAccountService.getAccountBalance(accountNumber)

        return when (bankAccountData) {
            null -> ResponseEntity(null, HttpStatus.NOT_FOUND)
            else -> ResponseEntity(
                GetBalanceResponse(
                    accountId = bankAccountData.accountId,
                    accountNumber = bankAccountData.accountNumber,
                    balance = bankAccountData.balance,
                    currency = bankAccountData.currency
                ), HttpStatus.OK
            )
        }
    }

    @PostMapping("/{accountNumber}/transfers")
    fun transferToAccount(
        @PathVariable("accountNumber") sourceAccountNumber : String,
        @RequestBody transferRequest: TransferAmountRequest
    ): ResponseEntity<TransferAmountResponse> {
        val bankAccountData = bankAccountService.transferFunds(
            sourceAccountNumber,
            transferRequest.targetAccountNumber,
            transferRequest.amount,
            transferRequest.currency
        )

        return when (bankAccountData) {
            null -> ResponseEntity(null, HttpStatus.NOT_FOUND)
            else -> ResponseEntity(
                TransferAmountResponse(
                    accountId = bankAccountData.accountId,
                    accountNumber = bankAccountData.accountNumber,
                    newBalanceAmount = bankAccountData.balance,
                    currency = bankAccountData.currency
                ), HttpStatus.OK
            )
        }
    }

    // Exception handling

    @ExceptionHandler(TransaferFailedException::class)
    fun handleFundsTransferExceptions(): ResponseEntity<HttpStatus> {
        return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(InsufficientFundsException::class)
    fun handleInsufficientFundsException(): ResponseEntity<HttpStatus> {
        return ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CannotTransferToSameAccount::class)
    fun handleTransfersToSameAccountException(): ResponseEntity<HttpStatus> {
        return ResponseEntity(HttpStatus.BAD_REQUEST);
    }
}