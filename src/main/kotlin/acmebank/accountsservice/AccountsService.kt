package acmebank.accountsservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootApplication
@EnableJpaRepositories(basePackages = ["acmebank.accountsservice.accounts.repositories", "acmebank.accountsservice.accounts.daos"])
class AccountsService

fun main(args: Array<String>) {
    runApplication<AccountsService>(*args)
}