CREATE TABLE IF NOT EXISTS users (
    user_id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(30) NOT NULL,
    password VARCHAR(256) NOT NULL,
    email VARCHAR(100) NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    address VARCHAR(255) NOT NULL,
    phone_number VARCHAR(30) NOT NULL,
    creation_ts TIMESTAMP NOT NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_users ON users(username);

CREATE TABLE IF NOT EXISTS accounts (
    account_id INT AUTO_INCREMENT PRIMARY KEY,
    account_number VARCHAR(15) NOT NULL,
    user_id INT NOT NULL,
    balance DECIMAL NOT NULL DEFAULT 0,
    currency VARCHAR(4) NOT NULL,
--    last_applied_trx_id INT NOT NULL,
    creation_ts TIMESTAMP NOT NULL,
    last_update_ts TIMESTAMP
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_account_number ON accounts(account_number);

CREATE TABLE IF NOT EXISTS transactions (
    trx_id INT AUTO_INCREMENT PRIMARY KEY,
    account_id INT NOT NULL,
    trx_type VARCHAR(10) NOT NULL,
    amount DECIMAL NOT NULL DEFAULT 0,
    target_account_id INT,
    creation_ts TIMESTAMP NOT NULL
);

CREATE INDEX IF NOT EXISTS idx_trx_by_account ON transactions(account_id, trx_type)