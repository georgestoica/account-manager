INSERT INTO users(
        username,
        password,
        email,
        first_name,
        last_name,
        address,
        phone_number,
        creation_ts
) VALUES (
    'johndoe',
    '1234',
    'johndoe@acmebank.com',
    'john',
    'doe',
    'Hong Kong',
    '+852111222',
    CURRENT_TIMESTAMP()
);

INSERT INTO users (
        username,
        password,
        email,
        first_name,
        last_name,
        address,
        phone_number,
        creation_ts
) VALUES (
    'janedoe',
    '1234',
    'jane@acmebank.com',
    'jane',
    'doe',
    'Singapore',
    '+65111222',
    CURRENT_TIMESTAMP()
);

INSERT INTO accounts(
    account_number,
    user_id,
    balance,
    currency,
    creation_ts
) VALUES (
    '12345678',
    1,
    1000000,
    'HKD',
    CURRENT_TIMESTAMP()
);

INSERT INTO accounts(
    account_number,
    user_id,
    balance,
    currency,
    creation_ts
) VALUES (
    '88888888',
    2,
    1000000,
    'HKD',
    CURRENT_TIMESTAMP()
);

INSERT INTO transactions (
    account_id,
    trx_type,
    amount,
    creation_ts
) VALUES (
    1,
    'DEPOSIT',
    1000000,
    CURRENT_TIMESTAMP()
);

INSERT INTO transactions (
    account_id,
    trx_type,
    amount,
    creation_ts
) VALUES (
    2,
    'DEPOSIT',
    1000000,
    CURRENT_TIMESTAMP()
);